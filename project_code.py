import sys

def find_occurrences(no_rows_motif, no_cols_motif, motif, no_rows_mosaic, no_cols_mosaic, mosaic):
    occurrences = []
    count = 0
    for i in range(no_rows_mosaic - no_rows_motif + 1):
        for j in range(no_cols_mosaic - no_cols_motif + 1):
            match = True
            for x in range(no_rows_motif):
                for y in range(no_cols_motif):
                    if motif[x][y] != 0 and motif[x][y] != mosaic[i + x][j + y]:
                        match = False
                        break
                if not match:
                    break
            if match:
                count += 1
                occurrences.append((i + 1, j + 1))
    result =  [(count,)] + occurrences
    for i in range(len(result)) :
        print(*result[i])

no_rows_motif, no_cols_motif = map(int, sys.argv[1:3])
motif = []
index = 3
for i in range(no_rows_motif):
    single_row_motif = list(map(int, sys.argv[index:index+no_cols_motif]))
    motif.append(single_row_motif)
    index += no_cols_motif

no_rows_mosaic, no_cols_mosaic = map(int, sys.argv[index:index+2])

mosaic = []
index += 2
for i in range(no_rows_mosaic):
    single_row_mosaic = list(map(int, sys.argv[index:index+no_cols_mosaic]))
    mosaic.append(single_row_mosaic)
    index += no_cols_mosaic
find_occurrences(no_rows_motif, no_cols_motif, motif, no_rows_mosaic, no_cols_mosaic, mosaic)
